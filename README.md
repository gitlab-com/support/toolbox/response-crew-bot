# Response Crew Bot

Current functionality:

## Remind the response crew each morning

This bot currently has one feature - it reminds the EMEA response crew in Slack
each morning by `@mentioning` them in the `#support_response-crew` channel.

A future iteration can remind crews in other regions.

### How it works

1. Run a script `reminder.rb` on a scheduled pipeline at 0503 UTC Monday to Friday.
1. The script gets the emails for today's crew from [Zapier storage](https://zapier.com/apps/storage/help) (See below for details)
1. Match the attendee emails with the Slack name in `support-team.yml`
1. Send a message to Slack `#support_response-crew` channel to remind the crew

## Zapier dependency

A scheduled Zap builds a list of emails for today's crew by querying Google calendar each weekday (Monday to Friday at 0300UTC) before the script here runs. It stores the list as JSON in [Zapier storage](https://zapier.com/apps/storage/help). Check the script and CI variables for how this is accessed.

The Zap is called `EMEA Response Crew Reminders` in the Support folder in the GitLab Zapier account.

We're using Zapier because it makes it easy to connect to Google calendar. Connecting to a non-public Google calendar from a Ruby script is not easy due to OAuth challenges.

We're not using Zapier for everything because it's not so easy to build the part that gets `support-team.yml` and match up emails and Slack IDs in Zapier. Hence this project does that part.

## Future enhancements

1. Add some error handling! Feel free to submit MRs
1. This currently relies on Zapier (see above for details). If someone is inspired to add this functionality to this project and remove the dependency on Zapier that could be useful iteration. If you want to work on that you'll need to be familiar with Google APIs and OAuth flows. The Response Crew Google calendar is owned by the `techsupport@gitlab.com` account - login details are in 1Password.

## Thanks

Making this was fast thanks to the work done by Lyle in the [frt-reminder](https://gitlab.com/gitlab-com/support/toolbox/frt-reminder) app. We're using a different Slack gem here for Ruby 2.7 compatibility.
