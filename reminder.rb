#!/usr/bin/ruby

require "http"
require "json"
require "yaml"
require 'slack/incoming/webhooks'

# load support-team.yml cloned in CI job
support = YAML.load_file("team/data/support-team.yaml")

# get the crew email addresses for today from Zapier store
url = "https://store.zapier.com/api/records?secret=" + ENV["ZAPIER_STORE_SECRET"]
uri = URI(url)
response = HTTP.get(uri)
the_crew_csv = JSON.parse(response)["the_crew"]

# build the list of slack ids to notify
people_to_notify = ""
the_crew = the_crew_csv.split(",")
the_crew.each do |p|
  if p == "kgrechishkina@gitlab.com"
    p = "khrechyshkina@gitlab.com"
  end
  slack_id = support.select{ |x| x['email'] == p }.first['slack']['id']
  rescue NoMethodError => error
    STDERR.puts("%s for '%s'. Is that in the support-team.yaml?" % [error, p])
  else
    people_to_notify = people_to_notify + " <@#{slack_id}>"
end

# Send the Slack notification to support_response-crew
slack_webhook = "https://hooks.slack.com/services/" + ENV["SLACK_WEBHOOK"]
slack = Slack::Incoming::Webhooks.new slack_webhook

slack.post "Good morning! %s If you're not available to help with the crew today, please swap with someone or let your manager know. Your goal is to reply to all tickets in the EMEA Needs Org/Triage/FRT view. Use this channel to collaborate!" % people_to_notify
